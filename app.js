let express = require('express');
let session = require('cookie-session'); // Charge le middleware de sessions
let bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
let urlencodedParser = bodyParser.urlencoded({ extended: false });
let app = express();
let port = process.env.PORT || 3000;
let server = app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
let io = require("socket.io")(server);


io.on("connection", (socket) => {
  socket.emit("newClient", { savedList: todolist });

  socket.on("newTask", (data) => {
    todolist.push(data.message);
    // io.emit("returnNewTask", { task: data });
    io.emit("returnNewTask", { list: todolist });
  });

  socket.on("taskToDelete", (data) => {
    todolist.splice(data.taskToDelete, 1);
    console.log(data);
    console.log(todolist);
    io.emit("returnNewTask", { list: todolist });
  });

});

todolist = [];

// Middlewares

app.set("view engine", "ejs");
app.set('views', './');
app.use(express.static('./'));

// Route

app.get('/', (req, res) => {
  res.render("index", { todolist: todolist });
});


